syntax = "proto3";
package cdm;
option java_package = "com.kitware.physiology.cdm";
option optimize_for = SPEED;

import "cdm/Properties.proto";

//   

/** @brief An active event, and how long it has been active */
message ActiveEventData
{
  eEvent         Event                       = 1; /**<< @ref eEngineEventTable. */
  ScalarTimeData Duration                    = 2; /**<< @brief The duration since last activated. */
}
message ActiveEventListData
{
  repeated ActiveEventData ActiveEvent       = 1;/**<< @brief Active Events and their duration. */
}

message EventChangeData
{
  eEvent         Event                       = 1; /**<< @ref eEngineEventTable. */
  ScalarTimeData SimTime                     = 2; /**<< @brief The simulation time the event change occured. */
  bool           Active                      = 3; /**<< @brief If the event is active or not. */
}
message EventChangeListData
{
  repeated EventChangeData Change            = 1; /**<< @ref Event changes. */
}

enum eEvent
{
  Antidiuresis                               = 0; /**<< @brief Low urine flow. */
  Asystole                                   = 1; /**<< @brief Represents no cardiac electrical activity. */
  Bradycardia                                = 2; /**<< @brief The heart rate is slowed to below 60 beats per minute. */
  Bradypnea                                  = 3; /**<< @brief The state at which the respiratory rate has fallen 10 breaths per minute. */     
  BrainOxygenDeficit                         = 4; /**<< @brief A lack of oxygen in the brain. Death will occur ~30min*/
  CardiacArrest                              = 5; /**<< @brief Sudden, unexpected loss of heart function, breathing, and consciousness.*/
  CardiogenicShock                           = 6; /**<< @brief Inadequate blood circulation due to failure of the heart ventricles (Cardiac Index < 2.2 L/min m2).*/
  CriticalBrainOxygenDeficit                 = 7; /**<< @brief A critical lack of oxygen in the brain. Death in under 10min*/
  Dehydration                                = 8; /**<< @brief A loss of more fluid than is taken in (More than 3% loss of resting fluid mass). */
  Diuresis                                   = 9; /**<< @brief High urine flow.*/
  Fasciculation                              = 10;/**<< @brief Brief spontaneous contractions of muscle fibers. */
  Fatigue                                    = 11;/**<< @brief The body is using energy above the Basal Metabolic Rate. */
  FunctionalIncontinence                     = 12;/**<< @brief Uncontrolled bladder release due to a full bladder. */
  Hypercapnia                                = 13;/**<< @brief State at which the arterial carbon dioxide partial pressure has exceeded 60 mmHg. */
  Hyperglycemia                              = 14;/**<< @brief An excess of glucose in the bloodstream (> 200 mg/dL). */
  Hyperthermia                               = 15;/**<< @brief The condition of having a body temperature greatly above normal (> 38 C). */
  Hypoglycemia                               = 16;/**<< @brief Low glucose in the bloodstream (< 70 mg/dL). */
  Hypothermia                                = 17;/**<< @brief The condition of having a body temperature greatly below normal (< 35 C). */
  Hypoxia                                    = 18;/**<< @brief State at which the arterial oxygen partial pressure has fallen below 65 mmHg. */       
  HypovolemicShock                           = 19;/**<< @brief The blood volume has dropped below 65% of its normal value. */
  IntracranialHypertension                   = 20;/**<< @brief Intracranial pressure is greater than 25 mmHg. */
  IntracranialHypotension                    = 21;/**<< @brief Intracranial pressure is lower than 7 mmHg. */
  IrreversibleState                          = 22;/**<< @brief An unrecoverable patient state. The engine will cease calculating when this event occurs. */
  Ketoacidosis                               = 23;/**<< @brief A form of metabolic acidosis where the anion gap is driven by the rise in ketones. */
  LacticAcidosis                             = 24;/**<< @brief A form of metabolic acidosis where the blood lactate concentration rises. */
  MaximumPulmonaryVentilationRate            = 25;/**<< @brief The maximum pulmonary ventilation rate has been reached. */
  MetabolicAcidosis                          = 26;/**<< @brief A condition where the body is producing excess acids. (pH < 7.35). */
  MetabolicAlkalosis                         = 27;/**<< @brief A condition where the body is producing excess bases. (pH > 7.45). */
  MyocardiumOxygenDeficit                    = 28;/**<< @brief The myocardium oxygen level has decreased below 5 mmHg. */
  Natriuresis                                = 29;/**<< @brief Sodium excretion above normal levels. */
  NutritionDepleted                          = 30;/**<< @brief The stomach is empty. */
  RenalHypoperfusion                         = 31;/**<< @brief Low blood flow to the kidneys. */
  RespiratoryAcidosis                        = 32;/**<< @brief An increase of CO2 concentration in the bloodstream and a decrease in blood pH. */
  RespiratoryAlkalosis                       = 33;/**<< @brief An decrease of CO2 concentration in the bloodstream and a increase in blood pH. */
  StartOfCardiacCycle                        = 34;/**<< @brief The Patient is starting a new heart beat. */
  StartOfExhale                              = 35;/**<< @brief Patient is starting to exhale. */
  StartOfInhale                              = 36;/**<< @brief Patient is starting to inhale. */
  Tachycardia                                = 37;/**<< @brief The heart rate is elevated above 100 beats per minute. */
  Tachypnea                                  = 38;/**<< @brief A breathing rate above 20 breaths per minute. */
  
  // Equipment
  AnesthesiaMachineOxygenBottleOneExhausted  = 1000;/**<< @brief Anesthesia machine oxygen bottle one has been exhausted. */
  AnesthesiaMachineOxygenBottleTwoExhausted  = 1001;/**<< @brief Anesthesia machine oxygen bottle two has been exhausted. */
  AnesthesiaMachineReliefValveActive         = 1002;/**<< @brief Anesthesia machine relief valve is active. */
  SupplementalOxygenBottleExhausted          = 1003;/**<< @brief The supplemental oxygen bottle is exhausted. There is no longer any oxygen to provide. */
  NonRebreatherMaskOxygenBagEmpty            = 1004;/**<< @brief The non rebreather mask oxygen bag is empty. Oxygen may need to be provided at a faster rate. */
}