syntax = "proto3";
package cdm;
option java_package = "com.kitware.physiology.cdm";
option optimize_for = SPEED;

import "cdm/Enums.proto";
import "cdm/Properties.proto";
import "cdm/AnesthesiaMachine.proto";
import "cdm/Actions.proto";

/** @brief Anesthesia machine action container. */
message AnyAnesthesiaMachineActionData
{
  oneof Action
  {
    AnesthesiaMachineConfigurationData                      Configuration               = 1;
    AnesthesiaMachineExpiratoryValveLeakData                ExpiratoryValveLeak         = 2;
    AnesthesiaMachineExpiratoryValveObstructionData         ExpiratoryValveObstruction  = 3;
    AnesthesiaMachineInspiratoryValveLeakData               InspiratoryValveLeak        = 4;
    AnesthesiaMachineInspiratoryValveObstructionData        InspiratoryValveObstruction = 5;
    AnesthesiaMachineMaskLeakData                           MaskLeak                    = 6;
    AnesthesiaMachineSodaLimeFailureData                    SodaLimeFailure             = 7;
    AnesthesiaMachineTubeCuffLeakData                       TubeCuffLeak                = 8;
    AnesthesiaMachineVaporizerFailureData                   VaporizerFailure            = 9;
    AnesthesiaMachineVentilatorPressureLossData             VentilatorPressureLoss      = 10;
    AnesthesiaMachineYPieceDisconnectData                   YPieceDisconnect            = 11;
    AnesthesiaMachineOxygenWallPortPressureLossData         OxygenWallPortPressureLoss  = 12;
    AnesthesiaMachineOxygenTankPressureLossData             OxygenTankPressureLoss      = 13;
  }
}

message AnesthesiaMachineActionData
{
  ActionData                               Action                      = 1;
}

/** @brief The configuration of the anesthesia machine to use. */
message AnesthesiaMachineConfigurationData
{
  AnesthesiaMachineActionData              AnesthesiaMachineAction     = 1;
  oneof Option
  {
    AnesthesiaMachineData                  Configuration               = 2;/**<< @brief An anesthesia machine object with properties to set in the system anesthesia machine. */
    string                                 ConfigurationFile           = 3;/**<< @brief File containing an anesthesia machine objet with properties to set in the system anesthesia machine. */
  }
}

/** @brief A leak in the expiritory valve. */
message AnesthesiaMachineExpiratoryValveLeakData
{
  AnesthesiaMachineActionData              AnesthesiaMachineAction     = 1;
  Scalar0To1Data                           Severity                    = 2;/**<< @brief Severity of the leak from 0 to 1. */
}

/** @brief An obstruction in the expiritory valve.  */
message AnesthesiaMachineExpiratoryValveObstructionData
{
  AnesthesiaMachineActionData              AnesthesiaMachineAction     = 1;
  Scalar0To1Data                           Severity                    = 2;/**<< @brief Severity of the obstruction from 0 to 1. */
}

/** @brief A leak in the inspiratory valve.  */
message AnesthesiaMachineInspiratoryValveLeakData
{
  AnesthesiaMachineActionData              AnesthesiaMachineAction     = 1;
  Scalar0To1Data                           Severity                    = 2;/**<< @brief Severity of the leak from 0 to 1. */
}

/** @brief An obstruction in the inspiritory valve. */
message AnesthesiaMachineInspiratoryValveObstructionData
{
  AnesthesiaMachineActionData              AnesthesiaMachineAction     = 1;
  Scalar0To1Data                           Severity                    = 2;/**<< @brief Severity of the obstruction from 0 to 1. */
}

/** @brief A leak in the mask. */
message AnesthesiaMachineMaskLeakData
{
  AnesthesiaMachineActionData              AnesthesiaMachineAction     = 1;
  Scalar0To1Data                           Severity                    = 2;/**<< @brief Severity of the leak from 0 to 1. */
}

/** @brief A soda lime failure. */
message AnesthesiaMachineSodaLimeFailureData
{
  AnesthesiaMachineActionData              AnesthesiaMachineAction     = 1;
  Scalar0To1Data                           Severity                    = 2;/**<< @brief Severity of the failure from 0 to 1. */
}

/** @brief A leak in the tube cuff. */
message AnesthesiaMachineTubeCuffLeakData
{
  AnesthesiaMachineActionData              AnesthesiaMachineAction     = 1;
  Scalar0To1Data                           Severity                    = 2;/**<< @brief Severity of the leak from 0 to 1. */
}

/** @brief The vaporizer fails. */
message AnesthesiaMachineVaporizerFailureData
{
  AnesthesiaMachineActionData              AnesthesiaMachineAction     = 1;
  Scalar0To1Data                           Severity                    = 2;/**<< @brief Severity of the failure from 0 to 1. */
}

/** @brief The ventilator losses pressure. */
message AnesthesiaMachineVentilatorPressureLossData
{
  AnesthesiaMachineActionData              AnesthesiaMachineAction     = 1;
  Scalar0To1Data                           Severity                    = 2;/**<< @brief Severity of the loss from 0 to 1. */
}

/** @brief The Y Piece becomes disconnected. */
message AnesthesiaMachineYPieceDisconnectData
{
  AnesthesiaMachineActionData              AnesthesiaMachineAction     = 1;
  Scalar0To1Data                           Severity                    = 2;/**<< @brief Severity of the leak from 0 to 1. */
}

/** @brief The wall port losses oxygen pressure. */
message AnesthesiaMachineOxygenWallPortPressureLossData
{
  AnesthesiaMachineActionData              AnesthesiaMachineAction     = 1;
  eSwitch                                  State                       = 2;/**<< @brief Turn the action on or off. */
}

/** @brief The active oxygen tank losses pressure. */
message AnesthesiaMachineOxygenTankPressureLossData
{
  AnesthesiaMachineActionData              AnesthesiaMachineAction     = 1;
  eSwitch                                  State                       = 2;/**<< @brief Turn the action on or off. */
}